////////////////////////////////////////////////////////////////////////////////
#include "WX11/XDisplay.hxx"
/******************************************************************************/
#include "WX11/Extensions/XRandr.hxx"
#include "Cxx/Utils/Log.hxx"
////////////////////////////////////////////////////////////////////////////////
extern "C" { using namespace X11::priv; }
namespace X11 {

_XDisplay     *Display::_dpy                 = nullptr;
Array<u32>    Display::_ignored_modifiers    = Array<u32>();
u32           Display::_state_cleaner        = 0;
u32           Display::_nscreens             = 0;
Array<Screen> Display::_screens              = Array<Screen>();

decltype(Display::IgnoredModifiers) Display::IgnoredModifiers;
decltype(Display::Name)             Display::Name;

// ~ ---------------------------------------------------------------------------
_invoke_static_constructor_(Display, 0)
_force_inline_ void
Display::StaticConstructor ()
{  LOG_DEBUG("Opening connection with XServer");
   if ( !(_dpy = ::XOpenDisplay(nullptr)) )
      LOG_ERROR("Unable to open a connection with XServer");

   LOG_INFO("Display Name: %s", Name);

   // ::: Get screen properties ::: //
   //RR::ScreenResources *xScnRsc = RR::GetScreenResources(DefaultRootWindow());
   //Screens.Reserve((NScreens = xScnRsc->NCrtc));
   //LOG_INFO("Number of screens: %u", NScreens);
   //
   //for ( u32 i = 0; i < NScreens; ++i )
   //{  RR::CrtcInfo *xScnInfo = RR::GetCrtcInfo(xScnRsc, xScnRsc->Crtcs[i]);
   //   //Screens.PushBack(XScreen(xScnInfo->X, xScnInfo->Y, xScnInfo->Width, xScnInfo->Height));
   //   / *LOG_INFO("Screen %d: x=%d, y=%d w=%u, h=%u", i, Screens[i].X, Screens[i].Y,
   //            Screens[i].Width, Screens[i].Height);* /
   //   RR::FreeCrtcInfo(xScnInfo);
   //}
   //RR::FreeScreenResources(xScnRsc);
}

_invoke_static_destructor_(Display, 0)
_force_inline_ void
Display::StaticDestructor ()
{  LOG_DEBUG("Closing connection with XServer");
   ::XCloseDisplay(_dpy);  // - We are sure that we have a connection with XServer
}

[[deprecated]] void
Display::SetScreensSnap (u32 /*snap*/)
{  //for ( u32 i = 0; i < NScreens; ++i )
   //   Screens[i].Snap = snap;
}

} // ~ namespace X11
