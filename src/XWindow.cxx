////////////////////////////////////////////////////////////////////////////////
#include "WX11/XWindow.hxx"
/******************************************************************************/
//#include "XLibIntern.hxx"
#include "WX11/XDisplay.hxx"
#include "WX11/Atoms.hxx"
////////////////////////////////////////////////////////////////////////////////
//extern "C"
//{  int    XMapWindow            (XDisplay *dpy, Window wnd);
//   int    XUnmapWindow          (XDisplay *dpy, Window wnd);
//   int    XSetWindowBorderWidth (XDisplay *dpy, Window wnd, unsigned int width);
//   int    XSetWindowBorder      (XDisplay *dpy, Window wnd, unsigned long border_pixel);
//   Status XGetWMProtocols       (XDisplay *dpy, Window wnd, Atom **protocols_return, int *count_return);
//   int    XMoveResizeWindow     (XDisplay *dpy, Window wnd, int x, int y, unsigned int width, unsigned int height);
//   int    XChangeProperty       (XDisplay *dpy, Window wnd, Atom property, Atom type, int format, int mode,
//                                 const unsigned char *data, int nelements);
//}

namespace X11 {

// ~ Construct & Destruct: --------------------------------------------------
XWindow::XWindow (XID id)
: _id(id)
{ }

XWindow::XWindow (XID id, int x, int y, u32 width, u32 height)
: _id(id)
, _x(x)
, _y(y)
, _width(width)
, _height(height)
{ }

XWindow::XWindow (XID id, int x, int y, u32 width, u32 height, u32 border_width)
: _id(id)
, _x(x)
, _y(y)
, _width(width)
, _height(height)
, _border_width(border_width)
{ }

XWindow::XWindow (XID id, int x, int y, u32 width, u32 height, bool visible)
: _id(id)
, _x(x)
, _y(y)
, _width(width)
, _height(height)
{  Visible = visible;  }

XWindow::XWindow(const XWindow &copy)
: _id(copy._id)
, _x(copy._x)
, _y(copy._y)
, _width(copy._width)
, _height(copy._height)
, _aspect(copy._aspect)
, _border_width(copy._border_width)
, _border_color(copy._border_color)
, _visible(copy._visible)
, _size_hints(copy._size_hints)
, _min_width(copy._min_width)
, _max_width(copy._max_width)
, _inc_width(copy._inc_width)
, _min_height(copy._min_height)
, _max_height(copy._max_height)
, _inc_height(copy._inc_height)
, _min_aspect(copy._min_aspect)
, _max_aspect(copy._max_aspect)
{ }

//•━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ < Operators > ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━•//
/*******************************************************************************************************************/
/** @brief
********************************************************************************************************************/
XWindow::operator Window (void) const
{  return _id;  }

bool
XWindow::operator == (Window wnd)
{  return _id == wnd;  }

bool
XWindow::operator == (XWindow const &xwnd)
{  return _id == xwnd._id;  }

bool
XWindow::operator != (Window wnd)
{  return _id != wnd;  }

bool
XWindow::operator != (XWindow const &xwnd)
{  return _id != xwnd._id;  }

// ~ Functions:
/*******************************************************************************************************************/
/** @brief Set the window size hints (size constraints)
 * @param xsh The 'XSizeHints' object wich contains the contraints
********************************************************************************************************************/
void
XWindow::SetSizeHints (XSizeHints &xsh)
{  if ( xsh.Flags & P_MIN_SIZE )
   {  //_size_hints |= X11::MIN_SIZE;
      _min_width  = xsh.MinWidth;
      _min_height = xsh.MinHeight;
   }
   else if ( xsh.Flags & P_BASE_SIZE )
   {  //_size_hints |= X11::MIN_SIZE;
      _min_width  = xsh.BaseWidth;
      _min_height = xsh.BaseHeight;
   }

   if ( xsh.Flags & P_MAX_SIZE )
   {  //_size_hints |= X11::MAX_SIZE;
      _max_width  = xsh.MaxWidth;
      _max_height = xsh.MaxHeight;
   }

   if ( xsh.Flags & P_RESIZE_INC )
   {  //_size_hints |= X11::RESIZE_INC;
      _inc_width  = xsh.WidthInc;
      _inc_height = xsh.HeightInc;
   }

   if ( xsh.Flags & P_ASPECT )
   {  //_size_hints |= X11::ASPECT;
      _min_aspect = float(xsh.MinAspect.X / xsh.MinAspect.Y);
      _max_aspect = float(xsh.MinAspect.Y / xsh.MinAspect.Y);
   }

   // ≡ Update the properties to adabt the new hints ≡ //
   Width  = _width;
   Height = _height;
}

/*******************************************************************************************************************/
/** @brief Overloads the function 'SetSizeHints()'
 * @param sh The size constraints of this window
********************************************************************************************************************/
/*void
XWindow::SetSizeHints (X11::SizeHints &)
{  //! @todo Complete 'XWindow::SetSizeHints()'
}*/

/*******************************************************************************************************************/
/** @brief Make the window visible
********************************************************************************************************************/
void
XWindow::Show (void)
{ Visible = true; }

/*******************************************************************************************************************/
/** @brief Make the window invisible
********************************************************************************************************************/
void
XWindow::Hide (void)
{ Visible = false; }

/*******************************************************************************************************************/
/** @brief Move the window on X, Y axis
 * @param x Position on X axis
 * @param y Position on Y axis
********************************************************************************************************************/
void
XWindow::Move (int x, int y)
{  X = x;
   Y = y;
}

void
XWindow::Resize (u32 const width, u32 const height)
{  Width  = width;
   Height = height;
}

void
XWindow::ResizeUL (u32 /*width*/, u32 /*height*/)
{  /*X = RX - (Width = width) - DoubleBorderWidth + 1;
   Y = BY - (Height = height) - DoubleBorderWidth + 1;*/
}

/*******************************************************************************************************************/
/** @brief Send and Atom property to the client
 * @param proto The property atom to send it to the client
 * @return      If the client can manage this atom
 *
 * @todo It's not logic a window send to her self a event, move this function to 'WM'
********************************************************************************************************************/
bool
XWindow::SendEvent (Atom /*proto*/)
{  /*int   count              = 0;
   Atom  *clientWMProtocols = nullptr;*/
   bool  exist              = false;

   // » Checking of the client has some atoms to handle wm protocols
   /*if ( XGetWMProtocols(Display::X, _id, &clientWMProtocols, &count) )
   {  while ( --count )  // » Cycle through the list of protocols to check if the wanted atom exist
      {  if ( clientWMProtocols[count] == proto )
         {  / *XEvent  xEvent {};
            xEvent.type                 = ClientMessage;
            xEvent.xclient.window       = _id;
            xEvent.xclient.message_type = X11::Atoms::WM_PROTOCOLS;
            xEvent.xclient.format       = 32;
            xEvent.xclient.data.l[0]    = proto;
            xEvent.xclient.data.l[1]    = CurrentTime;
            XSendEvent(Display::X, _id, false, NoEventMask, &xEvent);
            exist = true;
            break;* /
        }
      }
      XFree(clientWMProtocols);
   }*/
   return exist;
}

/*******************************************************************************************************************/
/** @brief Set client state
 * @param state State to set it.
 *
 * @todo It's not logic a window set a state to him self, move this function to 'Client'
********************************************************************************************************************/
void
XWindow::SetState (long /*state*/)
{  /*long  data[] = {state, 0};*/

   /*XChangeProperty(Display::X, _id, X11::Atoms::WM_STATE, X11::Atoms::WM_STATE, 32,
                   PROP_MODE_REPLACE, (u8 *)data, 2);*/
}

/*******************************************************************************************************************/
/** @brief Apply the buffered configurations
 *
 * @todo Create custom 'XWindowChanges' wich add the 'border_color' flag,
 *       Call 'XConfigureWindow()' to set {x, y, width, height, border_width},
 *       Check the 'border_color' flag
 *
 * @bug We apply direct without check if '_x' and '_y' are 'X11::Gravity', when creating the client
********************************************************************************************************************/
void
XWindow::ApplyChanges (void)
{  /*XMoveResizeWindow(Display::X, _id, _x, _y, _width, _height);
   XSetWindowBorderWidth(Display::X, _id, _border_width);
   XSetWindowBorder(Display::X, _id, _border_color);*/
}

/*******************************************************************************************************************/
/** @brief Return some informations about the window
********************************************************************************************************************/
/*const char*
XWindow::ToStr (void) const
{  _str.Clear();
   return _str << "XWindow: id= " << _id << " x=" << _x << " y=" << _y << "\n"
               << " width=" << _width << " height=" << _height << " borderWidth=" << _border_width;
}*/

} // ~ namespace X11
