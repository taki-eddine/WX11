////////////////////////////////////////////////////////////////////////////////
#include "WX11/XEvents.hxx"
/******************************************************************************/
#include <assert.h>
#include "WX11/XDisplay.hxx"
#include "WX11/XKey.hxx"
////////////////////////////////////////////////////////////////////////////////
namespace X11 {

namespace Event
{  using namespace Detail;

   namespace
   {  }

   namespace Detail
   {  XEvent          _init_order_(2)  _event                = { };
      Functor<void()> _init_order_(2)  _handlers[LAST_EVENT] = {nullptr};
      Global<bool>    _init_order_(2)  _run                  = true;

      Functor<void(XKeyPressEvent &)>         _init_order_(2)  KeyPressHandler         = nullptr;
      Functor<void(XKeyReleaseEvent &)>       _init_order_(2)  KeyReleaseHandler       = nullptr;
      Functor<void(XButtonPressEvent &)>      _init_order_(2)  ButtonPressHandler      = nullptr;
      Functor<void(XButtonReleaseEvent &)>    _init_order_(2)  ButtonReleaseHandler    = nullptr;
      Functor<void(XDestroyWindowEvent &)>    _init_order_(2)  DestroyNotifyHandler    = nullptr;
      Functor<void(XMapRequestEvent &)>       _init_order_(2)  MapRequestHandler       = nullptr;
      Functor<void(XConfigureRequestEvent &)> _init_order_(2)  ConfigureRequestHandler = nullptr;
      Functor<void(XClientMessageEvent &)>    _init_order_(2)  ClientMessageHandler    = nullptr;
      Functor<void(XMappingEvent &)>          _init_order_(2)  MappingNotifyHandler    = nullptr;

      void
      ConvertXlib ()
      {  switch ( _event.Type )
         {  case KEY_PRESS:         {  /*_event.KeyPress._sym = XkbKeycodeToKeysym(XDisplay::X, _event.KeyPress.Code, 0, 0);*/ break;  }
            case KEY_RELEASE:       {  /*_event.KeyPress._sym = XkbKeycodeToKeysym(XDisplay::X, _event.KeyPress.Code, 0, 0);*/ break;  }
            case BUTTON_PRESS:      {  /*_event.ButtonPress   = _xevent.xbutton;*/ break;  }
            case BUTTON_RELEASE:    {  /*_event.ButtonRelease = _xevent.xbutton;*/ break;  }
            case MOTION_NOTIFY:     {  /*_event.Motion        = _xevent.xmotion;*/ break;  }
            case ENTER_NOTIFY:      {  break;  }
            case LEAVE_NOTIFY:      {  break;  }
            case FOCUS_IN:          {  break;  }
            case FOCUS_OUT:         {  break;  }
            case KEYMAP_NOTIFY:     {  break;  }
            case EXPOSE:            {  break;  }
            case GRAPHICS_EXPOSE:   {  break;  }
            case NO_EXPOSE:         {  break;  }
            case VISIBILITY_NOTIFY: {  break;  }
            case CREATE_NOTIFY:     {  break;  }
            case DESTROY_NOTIFY:    {  break;  }
            case UNMAP_NOTIFY:      {  break;  }
            case MAP_NOTIFY:        {  break;  }
            case MAP_REQUEST:       {  /*_event.MapRequest    = _xevent.xmaprequest;*/ break;  }
            case REPARENT_NOTIFY:   {  break;  }
            case CONFIGURE_NOTIFY:  {  break;  }
            case CONFIGURE_REQUEST: {  break;  }
            case GRAVITY_NOTIFY:    {  break;  }
            case RESIZE_REQUEST:    {  break;  }
            case CIRCULATE_NOTIFY:  {  break;  }
            case CIRCULATE_REQUEST: {  break;  }
            case PROPERTY_NOTIFY:   {  break;  }
            case SELECTION_CLEAR:   {  break;  }
            case SELECTION_REQUEST: {  break;  }
            case SELECTION_NOTIFY:  {  break;  }
            case COLORMAP_NOTIFY:   {  break;  }
            case CLIENT_MESSAGE:    {  break;  }
            case MAPPING_NOTIFY:    {  break;  }
            case GENERIC_EVENT:     {  break;  }
         }
      }

      void
      KeyPressProxy ()
      {  if ( KeyPressHandler )
         {  /*KeyPressHandler(_event.KeyPress);*/  }
      }

      void
      KeyReleaseProxy ()
      {  if ( KeyReleaseHandler )
         {  /*KeyReleaseHandler(_event.KeyRelease);*/  }
      }

      void
      ButtonPressProxy ()
      {  if ( ButtonPressHandler )
         {  /*ButtonPressHandler(_event.ButtonPress);*/  }
      }

      void
      ButtonReleaseProxy ()
      {  if ( ButtonReleaseHandler )
         {  /*ButtonReleaseHandler(_event.ButtonRelease);*/  }
      }

      void
      DestroyNotifyProxy ()
      {  if ( DestroyNotifyHandler )
         {  /*DestroyNotifyHandler(_event.xdestroywindow);*/  }
      }

      void
      MapRequestProxy ()
      {  if ( MapRequestHandler )
         {  /*MapRequestHandler(_event.xmaprequest);*/  }
      }

      void
      ConfigureRequestProxy ()
      {  if ( ConfigureRequestHandler )
         {  /*ConfigureRequestHandler(_event.xconfigurerequest);*/  }
      }

      void
      ClientMessageProxy ()
      {  if ( ClientMessageHandler )
         {  /*ClientMessageHandler(_event.xclient);*/  }
      }

      void
      MappingNotifyProxy ()
      {  if ( MappingNotifyHandler )
         {  /*MappingNotifyHandler(_event.xmapping);*/  }
      }
   }

   constexpr char* const  EventNameToStr[LAST_EVENT] = { 0, 0, "KeyPress", "KeyRelease", "ButtonPress", "ButtonRelease", "MotionNotify", "EnterNotify", "LeaveNotify", "FocusIn", "FocusOut", "KeymapNotify", "Expose", "GraphicsExpose",
                                                         "NoExpose", "VisibilityNotify", "CreateNotify", "DestroyNotify", "UnmapNotify", "MapNotify", "MapRequest", "ReparentNotify", "ConfigureNotify", "ConfigureRequest", "GravityNotify", "ResizeRequest", "CirculateNotify", "CirculateRequest",
                                                         "PropertyNotify", "SelectionClear", "SelectionRequest", "SelectionNotify", "ColormapNotify", "ClientMessage", "MappingNotify", "GenericEvent"
                                                       };
   //Functor<XErrorHandler> _init_order_(2)  XErrorDefaultHandler;

   /*_static_block_order_(2)
   {  // ≡ Get the default XError handler ≡ //
      //SetErrorHandler((XErrorDefaultHandler = X11::Detail::XSetErrorHandler(nullptr)));
   }*/

   [[deprecated("Function under dev")]] void
   Handler ()
   {  /*
      LOG_DEBUG("Start handling events");
      CheckXErrors();

      while ( _run && !XNextEvent(XDisplay::X, (XEvent *)&_event) )
      {  LOG_DEBUG("Event occured: EventName=%s, EventCode=%d, HasHandler=%s", EventNameToStr[_xevent.type], Type, (_handlers[_xevent.type] ? "yes" : "no"));
         if ( _handlers[_event.Type] )
         {  ConvertXlib();
            _handlers[_event.Type]();
         }
      }*/
   }

   [[deprecated("Function under dev")]] bool
   Treat ()
   {  /*
      while ( _run && XPending(XDisplay::X) )
      {  XNextEvent(XDisplay::X, (XEvent *)&_event)
         LOG_DEBUG("Event occured: EventName=%s, EventCode=%d, HasHandler=%s", EventNameToStr[_xevent.type], Type, (_handlers[_xevent.type] ? "yes" : "no"));
         if ( _handlers[_event.Type] )
         {  ConvertXlib();
            _handlers[_event.Type]();
         }
      }*/
      return _run;
   }

   [[deprecated("Function under dev")]] void
   SetEventMask (Window /*wnd*/, long /*event_mask*/)
   {  /*XSelectInput(XDisplay::X, wnd, event_mask);*/  }

   [[deprecated("Function under dev")]] void
   MaskEvent (long const /*event_mask*/)
   {  /*
      XMaskEvent(XDisplay::X, event_mask, (XEvent *)&_event);
      ConvertXlib();
      */
   }

   [[deprecated("Function under dev")]] void
   ThrowEvent (long const /*event_mask*/)
   {  /*while ( XCheckMaskEvent(XDisplay::X, event_mask, (XEvent *)&_event) );*/  }

   void
   Quit ()
   { _run = false; }

   // → Key input
   [[deprecated("Function under dev")]] void
   RegisterKey (XID /*wnd*/, XID /*keysym*/, u32 /*mod*/)
   {  /*
      // Grab the key with the specified mod
      XGrabKey(XDisplay::X, XKeysymToKeycode(XDisplay::X, keysym), mod, wnd, true,
               GrabModeAsync, GrabModeAsync);

      // Regrab the key with the ignored mods
      for ( u32 i=0; XDisplay::IgnoredModifiers[i]; ++i)
      {  XGrabKey(XDisplay::X, XKeysymToKeycode(XDisplay::X, keysym), mod | XDisplay::IgnoredModifiers[i], wnd, true,
                  GrabModeAsync, GrabModeAsync);
      }*/
   }

   [[deprecated("Function under dev")]] void
   RegisterKey (XID /*wnd*/, XKeyAction &/*key_press_act*/)
   {  /*RegisterKey(wnd, key_press_act.Sym, key_press_act.Mod);*/  }

   [[deprecated("Function under dev")]] void
   RegisterKeyActionArray (XID /*wnd*/, const XKeyAction */*keys*/, u32 /*nkey*/)
   {  /*do
      {  --nkey;
         RegisterKey(wnd, keys[nkey].Sym, keys[nkey].Mod);
      } while ( nkey );*/
   }

   /**********************************************************************************************************************/
   /** @brief Registre any key and any modifier to selected window
    *  @todo Implement 'XKey::RegistreAnyKey(const WindowID wnd)'
   ***********************************************************************************************************************/
   [[deprecated("Function under dev")]] void
   RegistreAnyKey (const Window /*wnd*/)
   {  }

   [[deprecated("Function under dev")]] void
   UnregisterAnyKey (Window /*wnd*/)
   { /*XUngrabKey(XDisplay::X, AnyKey, AnyModifier, wnd);*/ }

   // → Button input
   [[deprecated("Function under dev")]] void
   RegistreButton (Window /*wnd*/, u32 /*button*/, u32 /*mod*/, int /*cursor_mode*/, int /*keyboard_mode*/)
   {  // ≡ Grab with and without locks  ≡ //
      /*for ( u32 const lock : LocksMask )
         XGrabButton(XDisplay::X, button, mod | lock, wnd, false,
                     ButtonPressMask, cursor_mode, keyboard_mode, None, None);*/
   }

   [[deprecated("Function under dev")]] void
   RegistreAnyButton (Window /*wnd*/, int /*cursor_mode*/, int /*keyboard_mode*/)
   {  // ≡ Grab any button ≡ //
      /*XGrabButton(XDisplay::X, AnyButton, AnyModifier, wnd, false,
                  ButtonPressMask, cursor_mode, keyboard_mode, None, None);*/
   }

   [[deprecated("Function under dev")]] void
   RegistreButtonAction(Window /*wnd*/, XButtonAction const */*btn_act*/, u32 /*nbtn*/, int /*cursor_mode*/, int /*keyboard_mode*/)
   {  /*do
      {  --nbtn;
         RegistreButton(wnd, btn_act[nbtn].Button, btn_act[nbtn].Mod, cursor_mode, keyboard_mode);
      } while ( nbtn );*/
   }

   [[deprecated("Function under dev")]] void
   UnregistreAnyButton (XID /*wnd*/)
   { /*XUngrabButton(XDisplay::X, AnyButton, AnyModifier, wnd);*/ }

   // → Error Handlers
   /*void
   SetXErrorHandler (XErrorHandler / *new_error_handler* /)
   {  / *X11::Detail::XSetErrorHandler(new_error_handler);* /  }*/

   [[deprecated("Function under dev")]] void
   CheckXErrors ()
   {  /*XSync(XDisplay::X, false);*/  }
}

} // ~ namespace X11
