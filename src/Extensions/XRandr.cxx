////////////////////////////////////////////////////////////////////////////////
#include "WX11/Extensions/XRandr.hxx"
/******************************************************************************/
#include "WX11/XDisplay.hxx"
////////////////////////////////////////////////////////////////////////////////
//extern "C"
//{  ScreenResources* XRRGetScreenResources  (XDisplay *dpy, Window window);
//   CrtcInfo*        XRRGetCrtcInfo         (XDisplay *dpy, ScreenResources *resources, Crtc crtc);
//   void             XRRFreeCrtcInfo        (CrtcInfo *crtcInfo);
//   void             XRRFreeScreenResources (ScreenResources *resources);
//}

namespace X11 {

namespace RR
{  // ~ [Public] ---------------------------------------------------------------
   void
   FreeScreenResources (ScreenResources */*resources*/)
   {  /*XRRFreeScreenResources(resources);*/  }

   void
   FreeCrtcInfo (CrtcInfo */*crtcInfo*/)
   {  /*XRRFreeCrtcInfo(crtcInfo);*/  }

   ScreenResources*
   GetScreenResources (Window /*window*/)
   {  return nullptr/*XRRGetScreenResources(Display::X, window)*/;  }

   CrtcInfo*
   GetCrtcInfo (ScreenResources */*resources*/, Crtc /*crtc*/)
   {  return nullptr/*XRRGetCrtcInfo(Display::X, resources, crtc)*/;  }
}

} // ~ namespace X11
