////////////////////////////////////////////////////////////////////////////////
#include "WX11/Screen.hxx"
////////////////////////////////////////////////////////////////////////////////
namespace X11 {

// ~ [Private] -----------------------------------------------------------------
/*int
Screen::GetMiddleX (void)
{ return _x + (_width / 2); }

int
Screen::GetMiddleY (void)
{ return _y + (_height / 2); }

int
Screen::GetRX (void)
{ return _x + _width; }

int
Screen::GetBY (void)
{ return _y + _height; }

int
Screen::GetSnapX (void)
{ return _x + _snap; }

int
Screen::GetSnapRX (void)
{ return _x + _width - _snap; }

int
Screen::GetSnapY (void)
{ return _y + _snap; }

int
Screen::GetSnapBY (void)
{ return _y + _height - _snap; }*/

// ~ [Public] ------------------------------------------------------------------
Screen::Screen (int x, int y, u32 width, u32 height)
: _x(x)
, _y(y)
, _width(width)
, _height(height)
, _snap(0)
{ }

Screen::Screen (Screen const &screen)
: _x(screen._x)
, _y(screen._y)
, _width(screen._width)
, _height(screen._height)
, _snap(screen._snap)
{ }

} // ~ namespace X11
