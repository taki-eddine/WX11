////////////////////////////////////////////////////////////////////////////////
#include "WX11/XCursor.hxx"
/******************************************************************************/
#include <assert.h>
#include "WX11/XDisplay.hxx"
#include "WX11/XEvents.hxx"
////////////////////////////////////////////////////////////////////////////////
namespace X11 {

namespace Pointer
{  namespace
   {  /*_static_block_order_(2)
      {  LOG_DEBUG("Create cursor fonts");
         //XDefineCursor(XDisplay::X, XDisplay::Root, Arrow);      // » Set the default cursor font
      }*/

      /*void
      OnScoupeOut (Global<Cursor> &cursor)
      { XFreeCursor(XDisplay::X, cursor); }*/
   }

   //! @todo Add new function 'LoadFont()' to replace 'XCreateFontCursor()'
   /*Global<Cursor> const _init_order_(2)  Arrow    = { XCreateFontCursor(XDisplay::X, XC_left_ptr), OnScoupeOut },
                        _init_order_(2)  Move     = { XCreateFontCursor(XDisplay::X, XC_hand1), OnScoupeOut },
                        _init_order_(2)  ResizeTL = { XCreateFontCursor(XDisplay::X, XC_ul_angle), OnScoupeOut },
                        _init_order_(2)  ResizeBR = { XCreateFontCursor(XDisplay::X, XC_lr_angle), OnScoupeOut };*/

   /****************************************************************************************************************/
   /** @brief Move the cursor to 'X' and 'Y' position
    * @param wnd The cursor will move depend on this window
    * @param x   The new 'X' coordinate
    * @param y   The new 'Y' coordinate
   *****************************************************************************************************************/
   [[deprecated]] void
   MoveTo (Window /*wnd*/, int /*x*/, int /*y*/)
   { /*XWarpPointer(XDisplay::X, None, wnd, 0, 0, 0, 0, x, y);*/ }

   /****************************************************************************************************************/
   /** @brief Move the cursor by a distance along the 'X' and 'Y' axis
    * @todo Add 'X', 'Y' members to track cursor position or call 'X' to get the current position
   *****************************************************************************************************************/
   [[deprecated]] void
   MoveBy (XWindow const &/*xwnd*/, int const &/*dx*/, int const &/*dy*/)
   {  }
}

} // ~ namespace X11
