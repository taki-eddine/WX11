////////////////////////////////////////////////////////////////////////////////
#include "WX11/X.hxx"
/******************************************************************************/
#include "WX11/XDisplay.hxx"
////////////////////////////////////////////////////////////////////////////////
namespace X11 {

/*_static_block_order_(2)
{   We are testing KeyMask::NUM_LOCK_MASK
   XModifierKeymap  *xModKeymap = XGetModifierMapping(XDisplay::X);            // » Holds All Display Modifier
   KeyCode          numlockCode = XKeysymToKeycode(XDisplay::X, XK_Num_Lock);  // » Get The Keycode of Numlock

   // ≡ Locks ≡ //
   if ( xModKeymap )
   {  // ≡ Search for the KeyCode in modifiers map ≡ //
       u32 i = 0, n = 8 * xModKeymap->max_keypermod;
       for ( ; i < n; ++i )
           if ( xModKeymap->modifiermap[i] == numlockCode )
               break;
       // ≡ we get the numlock index "i", "(i / xModKeymap->max_keypermod)" to get bit shift of modifier [1->8] ≡ //
       NumLockMask = (1 << (i / xModKeymap->max_keypermod));
       XFreeModifiermap(xModKeymap);
   }

   LocksMask[3] = (LocksMask[1] = LockMask) | (LocksMask[2] = NumLockMask);
}*/

Global<u32>  LocksMask[4] _init_ = {0};
Global<u32>  NumLockMask  _init_ = 0;

}  // ~ namespace X11
