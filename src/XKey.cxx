////////////////////////////////////////////////////////////////////////////////
#include "WX11/XKey.hxx"
/******************************************************************************/
#include "WX11/XDisplay.hxx"
////////////////////////////////////////////////////////////////////////////////
//extern "C"
//{  int XSetInputFocus (XDisplay *display, Window focus, int revert_to, Time time);
//}

namespace X11 {

namespace Keyboard
{  int
   SetInputFocus (Window /*focus*/, int /*revert_to*/, Time /*time*/)
   {  return 0/*XSetInputFocus(Display::X, focus, revert_to, time)*/;  }
}

/*
namespace Keys
{  u32 const     &Mod       = XEvents::InRun.xkey.state;
   KeySym        Keysym     = 0;
   Window const  &Wnd       = XEvents::InRun.xkey.window;
   Time const    &Timestamp = XEvents::InRun.xkey.time;
}
*/

} // ~ namespace X11
