////////////////////////////////////////////////////////////////////////////////
#include "WX11/XServer.hxx"
/******************************************************************************/
#include <X11/Xlib.h>
#include "Cxx/Utils/Log.hxx"
////////////////////////////////////////////////////////////////////////////////
namespace X11 {

// ~ [Public] ------------------------------------------------------------------
XServer::XServer ()
{  //LOG_DEBUG("Opening connection with XServer");
   //if ( !(*this = XOpenDisplay(nullptr)) )
   //   LOG_ERROR("Unable to open a connection with XServer");

   //LOG_INFO("Display Name: %s", DisplayName);
}

XServer::~XServer ()
{  //LOG_DEBUG("Closing connection with XServer");
   //XCloseDisplay(*this);  // » We are sure that we have a connection with XServer
}

// ~ ---------------------------------------------------------------------------
XServer::operator XDisplay * ()
{  return reinterpret_cast<XDisplay *>(_dpy);  }

} // ~ namespace X11
