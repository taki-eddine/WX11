////////////////////////////////////////////////////////////////////////////////
#include "WX11/Atoms.hxx"
/******************************************************************************/
#include "WX11/XDisplay.hxx"
////////////////////////////////////////////////////////////////////////////////

//extern "C"
//{  Atom XInternAtom (XDisplay *display, const char *atom_name, bool only_if_exists);
//}

namespace X11 {

namespace Atoms
{  const Atom
      // ≡ ICCCM Atoms ≡ //
      WM_PROTOCOLS               = XInternAtom("WM_PROTOCOLS"),
      WM_DELETE_WINDOW           = XInternAtom("WM_DELETE_WINDOW"),

      WM_STATE                   = XInternAtom("WM_STATE"),
      WM_STATE_WITHDRAWN         = 0,  // WithdrawnState
      WM_STATE_NORMAL            = 1,  // NormalState
      WM_STATE_ICONIC            = 3,  // IconicState

      WM_TAKE_FOCUS              = XInternAtom("WM_TAKE_FOCUS"),

      // ≡ EWMH Atoms ≡ //
      NET_WM_STATE               = XInternAtom("_NET_WM_STATE"),
      NET_WM_STATE_REMOVE        = 0,
      NET_WM_STATE_ADD           = 1,
      NET_WM_STATE_TOGGLE        = 2,
      NET_WM_STATE_FULLSCREEN    = XInternAtom("_NET_WM_STATE_FULLSCREEN"),

      NET_WM_NAME                = 0,

      NET_WM_WINDOW_TYPE         = XInternAtom("_NET_WM_WINDOW_TYPE"),
      NET_WM_WINDOW_TYPE_DIALOG  = 0,
      NET_WM_WINDOW_TYPE_DND     = 0,
      NET_WM_WINDOW_TYPE_UTILITY = 0,
      NET_WM_WINDOW_TYPE_DOCK    = 0,
      NET_WM_WINDOW_TYPE_TOOLBAR = 0,
      NET_WM_WINDOW_TYPE_MENU    = 0,
      NET_WM_WINDOW_TYPE_NORMAL  = 0,

      NET_WM_ALLOWED_ACTIONS     = 0,
      NET_WM_ACTION_MOVE         = 0,
      NET_WM_ACTION_RESIZE       = 0,
      NET_WM_ACTION_FULLSCREEN   = 0,
      NET_WM_ACTION_CLOSE        = 0,
      NET_WM_ACTION_MINIMIZE     = 0,

      NET_SUPPORTED              = XInternAtom("_NET_SUPPORTED"),
      NET_SUPPORTING_WM_CHECK    = 0,

      NET_ACTIVE_WINDOW          = 0,

      // ≡ Extra Atoms ≡ //
      UTF8_STRING                = 0;
}

// ~ ---------------------------------------------------------------------------
Atom
XInternAtom (const char */*name*/, const bool /*null_if_missing*/)
{  return 0/*XInternAtom(Display::X, name, null_if_missing)*/;  }

} // ~ namespace X11
