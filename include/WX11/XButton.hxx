////////////////////////////////////////////////////////////////////////////////
#pragma once
#include <functional>
#include "X.hxx"
#include "XDisplay.hxx"
////////////////////////////////////////////////////////////////////////////////
namespace X11 {

/*
namespace Button
{  class ButtonMap;

   extern u32 const     &Mod;
   extern u32 const     &Button;
   extern Window const  &Wnd;
   extern Time const    &Timestamp;

   void  RegistreAnyButton    (Window wnd, int cursor_mode, int keyboard_mode);
   void  RegistreButton       (u32 button, Window wnd, int cursor_mode, int keyboard_mode);
   void  RegistreButton       (u32 button, u32 mod, Window wnd, int cursor_mode, int keyboard_mode);
   void  RegistreButtonMap    (ButtonMap const *bmap, u32 nbtn, Window wnd, int cursor_mode, int keyboard_mode);
   //void  UnregistreButton (Uint32 const &button, Window const &wnd);
   void  UnregistreAnyButton  (Window wnd);

   _force_inline_ void
   CleanState (void)
   { const_cast<u32 &>(XEvents::InRun.xbutton.state) = X11::CleanState(XEvents::InRun.xbutton.state); }
}
*/

class XButtonEvent
{  public:
      int           Type;        /* of event */
      unsigned long Serial;      /* # of last request processed by server */
      bool          SendEvent;  /* true if this came from a SendEvent request */
   private:
      //XServer *_display;         /* Display the event was read from */
   public:
      Window       EventWindow;  /* "event" window it is reported relative to */
      Window       Root;         /* root window that the event occurred on */
      Window       SubWindow;    /* child window */
      Time         Timestamp;    /* milliseconds */
      int          X, Y;         /* pointer x, y coordinates in event window */
      int          XRoot, YRoot; /* coordinates relative to root */
      unsigned int State;        /* key or button mask */
      unsigned int Button;       /* detail */
      bool         SameScreen;   /* same screen flag */
};

class XButtonPressEvent
: public XButtonEvent
{  public:
};

class XButtonReleaseEvent
: public XButtonEvent
{  public:
};

class XButtonAction
{  public:
      u32                    Mod;
      u32                    Button;
      std::function<void()>  Function;

      XButtonAction (u32 mod, u32 button, std::function<void()> function)
      : Mod(mod)
      , Button(button)
      , Function(function)
      { }
};

} // ~ namespace X11
