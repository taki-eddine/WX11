////////////////////////////////////////////////////////////////////////////////
#pragma once
//#include <X11/Xlib.h>
//#include <X11/cursorfont.h>
#include "XWindow.hxx"
////////////////////////////////////////////////////////////////////////////////
namespace X11 {

namespace Pointer
{  namespace Detail
   {  extern Global<u32>  _x,
                          _y;
   }

   extern Global<Cursor> const  Arrow,
                                ResizeTL,
                                ResizeBR,
                                Move;

   void  MoveTo (Window wnd, int x, int y);

   [[ deprecated("Function not ready to use") ]]
   void  MoveBy (XWindow const &xwnd, int const &dx, int const &dy);
   /*
   extern struct X final
   {  constexpr
      X () { }

      _force_inline_
      operator u32 ()
      {  return;  }
   } X;

   extern struct Y final
   {  constexpr
      Y () { }

      _force_inline_
      operator u32 ()
      {  return;  }
   } Y;
   */
}

}  // ~ namespace X11
