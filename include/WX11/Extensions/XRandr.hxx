////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma once
#include "WX11/XLib.hxx"
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace X11 {

namespace RR
{  using Orientation = unsigned short;  // randr.h: Rotation
   using SizeID      = unsigned short;
   using Output      = XID;
   using Crtc        = XID;
   using RefreshRate = XID;             // randr.h: Mode
   using Provider    = XID;
   using ModeFlags   = unsigned long;

   /**
    * @brief Screen mode info
   **/
   class ModeInfo
   {  public:
         unsigned long ID;          //!< The id of the mode
         unsigned int  Width;       //!< Width in pixel
         unsigned int  Height;      //!< Height in pixel
         unsigned long DotClock;
         unsigned int  HSyncStart;
         unsigned int  HSyncEnd;
         unsigned int  HTotal;
         unsigned int  HSkew;
         unsigned int  VSyncStart;
         unsigned int  VSyncEnd;
         unsigned int  VTotal;
         char          *Name;
         unsigned int  NameLength;
         ModeFlags     Flags;
   };

   /**
    * @brief Screen resource
   **/
   class ScreenResources
   {  public:
         Time     Timestamp;
         Time     ConfigTimestamp;
         int      NCrtc;            // Number of screen
         Crtc     *Crtcs;           // List of screen id
         int      NOutput;          // ?
         Output   *Outputs;         // ?
         int      NMode;            // Number of modes
         ModeInfo *Modes;
   };

   /**
    * @brief Crtc (Screen resource) info
   **/
   class CrtcInfo
   {  public:
         Time          Timestamp;
         int           X, Y;            // Position
         unsigned int  Width, Height;   // Resolution
         unsigned long RefRate;         // Refresh Rate
         Orientation   Rotation;
         int           NOutput;
         Output        *Outputs;
         Orientation   Rotations;
         int           NPossible;
         Output        *Possible;
   };

   // ~ [Public] ---------------------------------------------------------------
   ScreenResources* GetScreenResources  (Window window);
   CrtcInfo*        GetCrtcInfo         (ScreenResources *resources, Crtc crtc);
   void             FreeCrtcInfo        (CrtcInfo *crtcInfo);
   void             FreeScreenResources (ScreenResources *resources);
}

} // ~ namespace X11
