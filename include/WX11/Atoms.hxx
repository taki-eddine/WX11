////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma once
#include "XDisplay.hxx"
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace X11 {

namespace Atoms
{  extern const Atom
      // ≡ ICCCM Atoms ≡ //
      WM_PROTOCOLS,                // ?
      WM_DELETE_WINDOW,            // To Check If the client has delete window atom, if he has we send him this atom

      WM_STATE,                    // The state of a client
      WM_STATE_WITHDRAWN,          // For windows that are not mapped
      WM_STATE_NORMAL,             // most applications want to start this way
      WM_STATE_ICONIC,             // application wants to start as an icon

      WM_TAKE_FOCUS,               // To tell To The Client That He Has The Focus

      // ≡ EWMH Atoms ≡ //
      NET_SUPPORTED,               // To Set The Atoms Supported By The Window Manager

      NET_WM_STATE,                // To Define if the message from a client is for change his state
      NET_WM_STATE_REMOVE,         // Remove This Property Directly
      NET_WM_STATE_ADD,            // Set This Property Directly
      NET_WM_STATE_TOGGLE,         // Toggle The State
      NET_WM_STATE_FULLSCREEN,     // To Defien if the client want be in fullscreen

      NET_WM_NAME,                 // Not sure: To Get The Name or Title of a client

      NET_WM_WINDOW_TYPE,          // Define Wich Window Type The Window Manager Can Handel it
      NET_WM_WINDOW_TYPE_DIALOG,   // Type Of Window is "Dialog window" (ex: sublime text buy dialog window)
      NET_WM_WINDOW_TYPE_DND,      // Type Of Window is "Drag and Drop" (ex: drag a folder or file in Thunar)
      NET_WM_WINDOW_TYPE_UTILITY,  // Type Of Window is "Utility" (ex: Toolbox)
      NET_WM_WINDOW_TYPE_DOCK,     // Type of WIndow is "Dock" (this window must stay in top)
      NET_WM_WINDOW_TYPE_TOOLBAR,  // Type of Window is "Toolbar"
      NET_WM_WINDOW_TYPE_MENU,     // Type of Window is "Menu"
      NET_WM_WINDOW_TYPE_NORMAL,   // Type of Window is "Normal" (regular window)

      NET_WM_ALLOWED_ACTIONS,
      NET_WM_ACTION_MOVE,
      NET_WM_ACTION_RESIZE,
      NET_WM_ACTION_FULLSCREEN,
      NET_WM_ACTION_CLOSE,
      NET_WM_ACTION_MINIMIZE,

      NET_SUPPORTING_WM_CHECK,

      NET_ACTIVE_WINDOW,           // ?

      // ≡ Extra Atoms ≡ //
      UTF8_STRING;                 // ?

   enum XAtom : Atom
   {  XA_PRIMARY             = 1,
      XA_SECONDARY           = 2,
      XA_ARC                 = 3,
      XA_ATOM                = 4,
      XA_BITMAP              = 5,
      XA_CARDINAL            = 6,
      XA_COLORMAP            = 7,
      XA_CURSOR              = 8,
      XA_CUT_BUFFER0         = 9,
      XA_CUT_BUFFER1         = 10,
      XA_CUT_BUFFER2         = 11,
      XA_CUT_BUFFER3         = 12,
      XA_CUT_BUFFER4         = 13,
      XA_CUT_BUFFER5         = 14,
      XA_CUT_BUFFER6         = 15,
      XA_CUT_BUFFER7         = 16,
      XA_DRAWABLE            = 17,
      XA_FONT                = 18,
      XA_INTEGER             = 19,
      XA_PIXMAP              = 20,
      XA_POINT               = 21,
      XA_RECTANGLE           = 22,
      XA_RESOURCE_MANAGER    = 23,
      XA_RGB_COLOR_MAP       = 24,
      XA_RGB_BEST_MAP        = 25,
      XA_RGB_BLUE_MAP        = 26,
      XA_RGB_DEFAULT_MAP     = 27,
      XA_RGB_GRAY_MAP        = 28,
      XA_RGB_GREEN_MAP       = 29,
      XA_RGB_RED_MAP         = 30,
      XA_STRING              = 31,
      XA_VISUALID            = 32,
      XA_WINDOW              = 33,
      XA_WM_COMMAND          = 34,
      XA_WM_HINTS            = 35,
      XA_WM_CLIENT_MACHINE   = 36,
      XA_WM_ICON_NAME        = 37,
      XA_WM_ICON_SIZE        = 38,
      XA_WM_NAME             = 39,
      XA_WM_NORMAL_HINTS     = 40,
      XA_WM_SIZE_HINTS       = 41,
      XA_WM_ZOOM_HINTS       = 42,
      XA_MIN_SPACE           = 43,
      XA_NORM_SPACE          = 44,
      XA_MAX_SPACE           = 45,
      XA_END_SPACE           = 46,
      XA_SUPERSCRIPT_X       = 47,
      XA_SUPERSCRIPT_Y       = 48,
      XA_SUBSCRIPT_X         = 49,
      XA_SUBSCRIPT_Y         = 50,
      XA_UNDERLINE_POSITION  = 51,
      XA_UNDERLINE_THICKNESS = 52,
      XA_STRIKEOUT_ASCENT    = 53,
      XA_STRIKEOUT_DESCENT   = 54,
      XA_ITALIC_ANGLE        = 55,
      XA_X_HEIGHT            = 56,
      XA_QUAD_WIDTH          = 57,
      XA_WEIGHT              = 58,
      XA_POINT_SIZE          = 59,
      XA_RESOLUTION          = 60,
      XA_COPYRIGHT           = 61,
      XA_NOTICE              = 62,
      XA_FONT_NAME           = 63,
      XA_FAMILY_NAME         = 64,
      XA_FULL_NAME           = 65,
      XA_CAP_HEIGHT          = 66,
      XA_WM_CLASS            = 67,
      XA_WM_TRANSIENT_FOR    = 68,
      XA_LAST_PREDEFINED     = 68
   };
}

//•━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ < Functions > ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━•//
Atom XInternAtom (const char *name, const bool null_if_missing=false);

} // ~ namespace X11
