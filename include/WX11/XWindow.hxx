////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma once
#include "Cxx/String.hxx"
#include "XLib.hxx"
#include "XUtil.hxx"
#include "Screen.hxx"
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace X11 {

class XWindow
{     using ClassName = XWindow;
   protected:
      XID     _id           = 0;
      int     _x            = 0,
              _y            = 0;
      u32     _width        = 1,
              _height       = 1;
      float   _aspect       = 0;
      u32     _border_width = 0;
      u64     _border_color = 0;
      bool    _visible      = false;

      long    _size_hints = 0;
      u32     _min_width  = 1,
              _max_width  = 0,
              _inc_width  = 0,
              _min_height = 1,
              _max_height = 0,
              _inc_height = 0;
      float   _min_aspect = 0,
              _max_aspect = 0;

      //! @todo Use 'WindowChanges' wich has the flags and the changes to apply them

      //mutable Cxx::String  _str;

   public:
      // ~ ---------------------------------------------------------------------
      XWindow (XID id);
      XWindow (XID id, int x, int y, u32 width, u32 height);
      XWindow (XID id, int x, int y, u32 width, u32 height, u32 border_width);
      XWindow (XID id, int x, int y, u32 width, u32 height, bool visible);
      XWindow (const XWindow &copy);

      // ~ ---------------------------------------------------------------------
      operator Window (void) const;
      bool  operator == (Window wnd);
      bool  operator == (XWindow const &xwnd);
      bool  operator != (Window wnd);
      bool  operator != (XWindow const &xwnd);

      // ~ ---------------------------------------------------------------------
      void        SetSizeHints  (XSizeHints &xsh);
      /*void        SetSizeHints  (X11::SizeHints &sh);*/
      void        Show          (void);
      void        Hide          (void);
      void        Move          (int x, int y);
      void        Resize        (u32 const width, u32 const height);
      void        ResizeUL      (u32 width, u32 height);
      bool        SendEvent     (Atom proto);
      void        SetState      (long state);
      void        ApplyChanges  (void);  //! @todo Rename 'ApplyChanges()' to 'Configure()'
      void        IgnoreChanges (void);  //! @todo Rename 'IgnoreChanges()' to 'ClearChanges()'

      //const char* ToStr         (void) const;

      // ~ ---------------------------------------------------------------------
      _property_(X,           _x,            get);
      _property_(Y,           _y,            get);
      _property_(Width,       _width,        get);
      _property_(Height,      _height,       get);
      //_property_(BorderWidth, _border_width, get);
      _property_(BorderColor, _border_color, get);
      _property_(Visible,     _visible,      get);
      //PROPERTY(SizeHints,   _size_hints,   GET);

    /*_closure_(int, RX      );
      _closure_(int, BY      );
      _closure_(int, InnerX  );
      _closure_(int, InnerY  );
      _closure_(int, InnerRX );
      _closure_(int, InnerBY );
      _closure_(u32, WidthBounds       );
      _closure_(u32, HeightBounds      );
      _closure_(u32, DoubleBorderWidth );*/
};

// ~ ---------------------------------------------------------------------------
void
XWindow::setX (const int &/*x*/)
{  /*switch ( x )
   {  case X11::Gravity::Center:
      {  _x = XDisplay::Screens[0].MiddleX - (WidthBounds / 2);
         break;
      }
      case X11::Gravity::Right:
      {  //! @todo move the window to right of the screen
         break;
      }
      case X11::Gravity::Left:
      {  _x = 0;
         break;
      }
      default:
      { _x = x; }
   }*/
}

void
XWindow::setY (const int &/*y*/)
{  /*switch ( y )
   {  case X11::Gravity::Center:
      {  _y = XDisplay::Screens[0].MiddleY - (HeightBounds / 2);
         break;
      }
      case X11::Gravity::Top:
      {  _y = 0;
         break;
      }
      case X11::Gravity::Bottom:
      {  //! @todo move the window to bottom of the screen
         break;
      }
      default:
      { _y = y; }
   }*/
}
/*
int
XWindow::GetInnerX (void)
{ return _x + _border_width; }

int
XWindow::GetInnerY (void)
{ return _y + _border_width; }

int
XWindow::GetRX (void)
{ return _x + _width + DoubleBorderWidth - 1; }

int
XWindow::GetBY (void)
{ return _y + _height + DoubleBorderWidth - 1; }

int
XWindow::GetInnerRX (void)
{ return _x + _width + _border_width - 1; }

int
XWindow::GetInnerBY (void)
{ return _y + _height + _border_width - 1; }
*/

void
XWindow::setWidth (const u32 &width)
{  /*if ( (_size_hints & X11::MIN_SIZE) && (width < _min_width) )
   {  width = _min_width;  }
   else if ( (_size_hints & X11::MAX_SIZE) && (width > _max_width) )
   {  width = _max_width;  }

   if ( _size_hints & X11::RESIZE_INC )
   {  width -= width % _inc_width;  }

   if ( _size_hints & X11::ASPECT )
   {  / *_aspect = float(width / _height);
      // ≡ Fix the height and keep the width ≡ //
      if ( _aspect < _minAspect )
         _height = width / (_aspect = _minAspect);
      else if ( _aspect > _maxAspect )
         _height = width / (_aspect = _maxAspect);* /
   }*/

   _width = width;
}

/*
u32
XWindow::GetWidthBounds (void)
{ return _width + DoubleBorderWidth; }
*/

void
XWindow::setHeight (const u32 &height)
{  /*if ( (_size_hints & X11::MIN_SIZE) && (height < _min_height) )
   {  height = _min_height;  }
   else if ( (_size_hints & X11::MAX_SIZE) && (height > _max_height) )
   {  height = _max_height;  }

   if ( _size_hints & X11::RESIZE_INC )
   {  height -= height % _inc_height;  }

   if ( _size_hints & X11::ASPECT )
   {  / *_aspect = float(_width / _height);
      // ≡ Fix the width and keep the height ≡ //
      if ( _aspect < _minAspect )
         _width  = _height * (_aspect = _minAspect);
      else if ( _aspect > _maxAspect )
         _width  = _height * (_aspect = _maxAspect);* /
   }*/

   _height = height;
}

/*
u32
XWindow::GetHeightBounds (void)
{ return _height + DoubleBorderWidth; }
*/

void
XWindow::setVisible (const bool &/*visible*/)
{  /*(_visible = visible) ? XMapWindow(Display::X, _id)
                        : XUnmapWindow(Display::X, _id);*/
}
/*
void
XWindow::SetBorderWidth (u32 border_width)
{  XSetWindowBorderWidth(Display::X, _id, (_border_width = border_width));  }

u32
XWindow::GetDoubleBorderWidth (void)
{ return _border_width * 2; }
*/

void
XWindow::setBorderColor (const u64 &/*border_color*/)
{  /*XSetWindowBorder(Display::X, _id, (_border_color = border_color));*/  }

} // ~ namespace X11
