////////////////////////////////////////////////////////////////////////////////
#pragma once
#include "XLib.hxx"
#include "Screen.hxx"
////////////////////////////////////////////////////////////////////////////////
namespace X11 {

class XServer
{     using ClassName = XServer;
      typedef struct _XDisplay XDisplay;
   public:
      /**
       * @brief Information about the screen.
       * The contents of this structure are implementation dependent.
       * A Screen should be treated as opaque by application code.
      **/
      //struct Screen /*Xlib: Screen*/
      //{  XExtData       *_ext_data;            // hook for extension to hang data
      //   XServer        *_display;             // back pointer to display structure
      //   Window         _root;                 // Root window id.
      //   int            _width,
      //                  _height;               // width and height of screen
      //   int            _mwidth,
      //                  _mheight;              // width and height of  in millimeters
      //   int            _ndepths;              // number of depths possible
      //   XDepth         *_depths;              // list of allowable depths on the screen
      //   int            _root_depth;           // bits per pixel
      //   Visual         *_root_visual;         // root visual
      //   GC             _default_gc;           // GC for the root root visual
      //   Colormap       _cmap;                 // default color map
      //   unsigned long  _white_pixel;
      //   unsigned long  _black_pixel;          // White and Black pixel values
      //   int            _max_maps, _min_maps;  // max and min color maps
      //   int            _backing_store;        // Never, WhenMapped, Always
      //   bool           _save_unders;
      //   long           _root_input_mask;      // initial root input mask
      //};

      /**
       * @brief Format structure; describes ZFormat data the screen will understand.
      **/
      //struct ScreenFormat /*Xlib: ScreenFormat*/
      //{  XExtData  *ExtData;      // hook for extension to hang data
      //   int       Depth;         // depth of this image format
      //   int       BitsPerPixel;  // bits/pixel at this depth
      //   int       ScanlinePad;   // scanline must padded to this multiple
      //};

   private:
      XDisplay *_dpy;

   public:
      // ~ ---------------------------------------------------------------------
      XServer  ();
      ~XServer ();

      // ~ ---------------------------------------------------------------------
      operator XDisplay * ();

      // ~ ---------------------------------------------------------------------
      //_property_(_dpy->DefaultScreen, DefaultScreen, _get_);

      //_array_property_(_dpy->DisplayName,   DisplayName);
      //_array_property_(_dpy->Screens,       Screens);
};

}  // ~ namespace X11
