////////////////////////////////////////////////////////////////////////////////
#pragma once
#include "XLib.hxx"
////////////////////////////////////////////////////////////////////////////////
namespace X11 {

/**
 * @brief Physical Screen of the XServer display
**/
class Screen
{     using ClassName = Screen;
   private:
      // ~ ---------------------------------------------------------------------
      int _x      = 0;  //!< X position
      int _y      = 0;  //!< Y position
      int _width  = 1;  //!< Width in pixel of screen
      int _height = 1;  //!< Height in pixel of screen
      u32 _snap   = 0;

   public:
      // ~ ---------------------------------------------------------------------
      Screen (int x, int y, u32 width, u32 height);
      Screen (Screen const &screen);

      // ~ ---------------------------------------------------------------------
      _property_(X,      _x,      get, set);
      _property_(Y,      _y,      get, set);
      _property_(Width,  _width,  get, set);
      _property_(Height, _height, get, set);
      _property_(Snap,   _snap,   get, set);
   /*
      _property_(int, MiddleX, _get_, _read_only_);
      _property_(int, MiddleY, _get_, _read_only_);
      _property_(int, RX     , _get_, _read_only_);
      _property_(int, BY     , _get_, _read_only_);
      _property_(int, SnapX  , _get_, _read_only_);
      _property_(int, SnapRX , _get_, _read_only_);
      _property_(int, SnapY  , _get_, _read_only_);
      _property_(int, SnapBY , _get_, _read_only_);
   */
};

} // ~ namespace X11
