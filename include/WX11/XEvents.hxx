////////////////////////////////////////////////////////////////////////////////
#pragma once
#include "XLib.hxx"
#include "XKey.hxx"
#include "XButton.hxx"
////////////////////////////////////////////////////////////////////////////////
namespace X11 {

//•━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ < Definitions Of Specific Events > ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━•//
class XAnyEvent
{  public:
      int           Type;
      unsigned long Serial;     /* # of last request processed by server */
      bool          SendEvent;  /* true if this came from a SendEvent request */
   private:
      //XServer       *_display;  /* Display the event was read from */
   public:
      Window        EventWindow;  /* window on which event was requested in event mask */
};

class XMotionEvent
{  public:
      int           Type;          /* of event */
      unsigned long Serial;        /* # of last request processed by server */
      bool          SendEvent;     /* true if this came from a SendEvent request */
   private:
      //XServer       *_display;     /* Display the event was read from */
   public:
      Window        EventWindow;   /* "event" window reported relative to */
      Window        Root;          /* root window that the event occurred on */
      Window        SubWindow;     /* child window */
      Time          Timestamp;     /* milliseconds */
      int           X, Y;          /* pointer x, y coordinates in event window */
      int           XRoot, YRoot;  /* coordinates relative to root */
      unsigned int  Mod;           /* key or button mask */
      char          IsHint;        /* detail */
      bool          SameScreen;    /* same screen flag */
};

//XCrossingEvent xcrossing;
//XFocusChangeEvent xfocus;
//XExposeEvent xexpose;
//XGraphicsExposeEvent xgraphicsexpose;
//XNoExposeEvent xnoexpose;
//XVisibilityEvent xvisibility;
//XCreateWindowEvent xcreatewindow;

class XDestroyWindowEvent
{  public:
      int           type;
      unsigned long serial;   /* # of last request processed by server */
      bool          send_event;   /* true if this came from a SendEvent request */
   private:
      //XServer *_display;   /* Display the event was read from */
   public:
      Window event;
      Window window;
};

//XUnmapEvent xunmap;
//XMapEvent xmap;

class XMapRequestEvent
{  public:
      int           Type;
      unsigned long Serial;     /* # of last request processed by server */
      bool          SendEvent;  /* true if this came from a SendEvent request */
   private:
      //XServer       *_display;  /* Display the event was read from */
   public:
      Window        ParentWnd;
      Window        EventWindow;
};

//XReparentEvent xreparent;
//XConfigureEvent xconfigure;
//XGravityEvent xgravity;
//XResizeRequestEvent xresizerequest;

class XConfigureRequestEvent
{  public:
      int           Type;
      unsigned long Serial;     /* # of last request processed by server */
      bool          SendEvent;  /* true if this came from a SendEvent request */
   private:
      //XServer *_display;        /* Display the event was read from */
   public:
      Window        Parent;
      Window        EventWindow;
      int           X, Y;
      int           Width, Height;
      int           BorderWidth;
      Window        Above;
      int           Detail;      /* Above, Below, TopIf, BottomIf, Opposite */
      unsigned long ValueMask;
};

//XCirculateEvent xcirculate;
//XCirculateRequestEvent xcirculaterequest;
//XPropertyEvent xproperty;
//XSelectionClearEvent xselectionclear;
//XSelectionRequestEvent xselectionrequest;
//XSelectionEvent xselection;
//XColormapEvent xcolormap;

class XClientMessageEvent
{  public:
      int           Type;
      unsigned long Serial;   /* # of last request processed by server */
      bool          SendEvent;   /* true if this came from a SendEvent request */
   private:
      //XServer *_display;   /* Display the event was read from */
   public:
      Window   EventWindow;
      Atom     MessageType;
      int      Format;
      union
      {  char  B[20];
         short S[10];
         long  L[5];
      } Data;
};

class XMappingEvent
{  public:
      int           Type;
      unsigned long Serial;      /* # of last request processed by server */
      bool          SendEvent;   /* true if this came from a SendEvent request */
   private:
      //XServer *_display;      /* Display the event was read from */
   public:
      Window   EventWindow;   /* unused */
      int      Request;       /* one of MappingModifier, MappingKeyboard, MappingPointer */
      int      FirstKeycode;  /* first keycode */
      int      Count;         /* defines range of change w. first_keycode*/
};

class XErrorEvent
{  public:
      int Type;
   private:
      //XServer *_display;   /* Display the event was read from */
   public:
      XID           ResourceID;  /* resource id */
      unsigned long Serial;      /* serial number of failed request */
      unsigned char ErrorCode;   /* error code of failed request */
      unsigned char RequestCode; /* Major op-code of failed request */
      unsigned char MinorCode;   /* Minor op-code of failed request */
};

//XKeymapEvent xkeymap;
//XGenericEvent xgeneric;
//XGenericEventCookie xcookie;

union XEvent
{  public:
      int                    Type;             /* must not be changed; first element */
      XAnyEvent              Any;
      XKeyEvent              Key;
      XButtonEvent           Button;
      XMotionEvent           Motion;
      //XCrossingEvent         Crossing;
      //XFocusChangeEvent      Focus;
      //XExposeEvent           Expose;
      //XGraphicsExposeEvent   GraphicsExpose;
      //XNoExposeEvent         NoExpose;
      //XVisibilityEvent       Visibility;
      //XCreateWindowEvent     CreateWindow;
      XDestroyWindowEvent    DestroyWindow;
      //XUnmapEvent            Unmap;
      //XMapEvent              Map;
      XMapRequestEvent       MapRequest;
      //XReparentEvent         Reparent;
      //XConfigureEvent        Configure;
      //XGravityEvent          Gravity;
      //XResizeRequestEvent    ResizeRequest;
      XConfigureRequestEvent ConfigureRequest;
      //XCirculateEvent        Circulate;
      //XCirculateRequestEvent CirculateRequest;
      //XPropertyEvent         Property;
      //XSelectionClearEvent   SelectionClear;
      //XSelectionRequestEvent SelectionRequest;
      //XSelectionEvent        Selection;
      //XColormapEvent         Colormap;
      XClientMessageEvent    Client;
      XMappingEvent          Mapping;
      //XErrorEvent            Error;
      //XKeymapEvent           Keymap;
      //XGenericEvent          Generic;
      //XGenericEventCookie    Cookie;

   private:
      long                   _pad[24];
};

namespace Event
{  namespace Detail
   {  extern XEvent _event;

      extern Functor<void()> _handlers[LAST_EVENT];
      extern Global<bool>    _run;

      extern Functor<void(XKeyPressEvent &)>         KeyPressHandler;
      extern Functor<void(XKeyReleaseEvent &)>       KeyReleaseHandler;
      extern Functor<void(XButtonPressEvent &)>      ButtonPressHandler;
      extern Functor<void(XButtonReleaseEvent &)>    ButtonReleaseHandler;
      extern Functor<void(XDestroyWindowEvent &)>    DestroyNotifyHandler;
      extern Functor<void(XMapRequestEvent &)>       MapRequestHandler;
      extern Functor<void(XConfigureRequestEvent &)> ConfigureRequestHandler;
      extern Functor<void(XClientMessageEvent &)>    ClientMessageHandler;
      extern Functor<void(XMappingEvent &)>          MappingNotifyHandler;

      void  ConvertXlib ();

      void  KeyPressProxy         ();
      void  KeyReleaseProxy       ();
      void  ButtonPressProxy      ();
      void  ButtonReleaseProxy    ();
      void  DestroyNotifyProxy    ();
      void  MapRequestProxy       ();
      void  ConfigureRequestProxy ();
      void  ClientMessageProxy    ();
      void  MappingNotifyProxy    ();
   }

   extern char* const  EventNameToStr[LAST_EVENT];
   //extern Functor<int()(Display*, XErrorEvent*)> XErrorDefaultHandler;    // » The default error handler of XLib

   void  Handler      ();
   bool  Treat        ();
   void  SetEventMask (Window wnd, long event_mask);
   void  MaskEvent    (long const event_mask);
   void  ThrowEvent   (long const event_mask);
   void  Quit         ();

   // → Registration of handlers
   void  RegisterEventHandler (void (*fn)(XKeyPressEvent &));
   void  RegisterEventHandler (void (*fn)(XKeyReleaseEvent &));
   void  RegisterEventHandler (void (*fn)(XButtonPressEvent &));
   void  RegisterEventHandler (void (*fn)(XButtonReleaseEvent &));
   void  RegisterEventHandler (void (*fn)(XDestroyWindowEvent &));
   void  RegisterEventHandler (void (*fn)(XMapRequestEvent &));
   void  RegisterEventHandler (void (*fn)(XConfigureRequestEvent &));
   void  RegisterEventHandler (void (*fn)(XClientMessageEvent &));
   void  RegisterEventHandler (void (*fn)(XMappingEvent &));

   template<typename FnSignature, bool Assert=false>
     void  RegisterEventHandler (FnSignature);

   template<bool Assert=false>
     void RegisterEventHandler (void);

   template<typename FnSignature, typename ... Args>
     void RegisterEventHandler (FnSignature fn, Args ... args);

   // → Registration of input devices
   // → Key input
   void  RegisterKey (XID wnd, XID keysym, u32 mod);
   void  RegisterKey (XID wnd, XKeyAction &);
   void  RegisterKeyActionArray (XID wnd, const XKeyAction *keymap, u32 nkey);
   void  UnregisterAnyKey (Window wnd);

   // → Button input
   void  RegistreButton (Window wnd, u32 button, u32 mod, int cursor_mode, int keyboard_mode);
   void  RegistreAnyButton (Window wnd, int cursor_mode, int keyboard_mode);
   void  RegistreButtonAction(Window wnd, XButtonAction const *btn_act, u32 nbtn, int cursor_mode, int keyboard_mode);
   void  UnregistreAnyButton (XID wnd);

   // → Error Handlers
   void  CheckXErrors     (void);
   //void  SetXErrorHandler (XErrorHandler newErrorHandler);

   // → Accessors
   //_space_property_(Detail::_event.Type,     Type);
   //_space_property_(Detail::_event.Button,   ButtonRelease);
   //_space_property_(Detail::_event.Motion,   Motion);
}

} // ~ namespace X11

////////////////////////////////////////////////////////////////////////////////
#include "XEvents.txx"
////////////////////////////////////////////////////////////////////////////////
