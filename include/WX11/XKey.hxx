////////////////////////////////////////////////////////////////////////////////
#pragma once
#include <functional>
#include "XDisplay.hxx"
////////////////////////////////////////////////////////////////////////////////
namespace X11 {

namespace Keyboard
{  // @todo Rename it to 'SetFocusOwner()'
   int SetInputFocus (Window focus, int revert_to, Time time);
}

/*
 //! @todo We will implemented when we need Async access to key state
namespace Keys
{  _private_space_
   {  //extern bool  _state[256];  // » keycode array
      //extern u32   _modifier;
   }

   void  UpdateState (KeyPressEvent &);
   void  UpdateState (KeyReleaseEvent &);

   extern struct Mod final
   {  constexpr
      Mod () { }

      _force_inline_
      operator u32 ()
      {  using _private_space_;
         return _modifier;
      }
   } Mod;

   extern struct State final
   {  constexpr
      State () { }

      _force_inline_
      bool
      operator [] (u32 sym)
      {  using _private_space_;
         return _state[sym];
      }
   } State;

}
*/

// This declaration only to allow friend
namespace Event { namespace Detail { void ConvertXlib (); } }

class XKeyEvent
{  public:
      int           Type;         /* of event */
      unsigned long Serial;       /* # of last request processed by server */
      bool          SendEvent;    /* true if this came from a SendEvent request */
   private:
      //XServer *_display;          /* Display the event was read from */
   public:
      Window       EvntWindow;    /* "event" window it is reported relative to */
      Window       Root;          /* root window that the event occurred on */
      Window       Subwindow;     /* child window */
      Time         Timestamp;     /* milliseconds */
      int          X, Y;          /* pointer x, y coordinates in event window */
      int          XRoot, YRoot;  /* coordinates relative to root */
      unsigned int State;         /* key or button mask */
      unsigned int Keycode;       /* detail */
      bool         SameScreen;    /* same screen flag */
};

class XKeyPressEvent
: public XKeyEvent
{  public:
};

class XKeyReleaseEvent
: public XKeyEvent
{  public:
};

class XKeyAction
{  public:
      u32                    Mod;
      u64                    Sym;
      std::function<void()>  Function;

      XKeyAction (u32 mod, u64 sym, std::function<void()> function)
      : Mod(mod)
      , Sym(sym)
      , Function(function)
      { }
};

} // ~ namespace X11
