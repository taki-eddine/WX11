////////////////////////////////////////////////////////////////////////////////
#include "XEvents.hxx"
////////////////////////////////////////////////////////////////////////////////
namespace X11 {

namespace Event
{  _force_inline_ void
   RegisterEventHandler (void (*fn)(XKeyPressEvent &))
   {  using namespace Detail;
      _handlers[KEY_PRESS] = KeyPressProxy;
      KeyPressHandler      = fn; /* std::cref(InRun.xkey): use Xkey*/
   }

   _force_inline_ void
   RegisterEventHandler (void (*fn)(XKeyReleaseEvent &))
   {  using namespace Detail;
      _handlers[KEY_RELEASE] = KeyReleaseProxy;
      KeyReleaseHandler      = fn; /* std::cref(InRun.xkey): use Xkey*/
   }

   _force_inline_ void
   RegisterEventHandler (void (*fn)(XButtonPressEvent &))
   {  using namespace Detail;
      _handlers[BUTTON_PRESS] = ButtonPressProxy;
      ButtonPressHandler      = fn; /* std::cref(InRun.xbutton): use XButton */
   }

   _force_inline_ void
   RegisterEventHandler (void (*fn)(XButtonReleaseEvent &))
   {  using namespace Detail;
      _handlers[BUTTON_RELEASE] = ButtonReleaseProxy;
      ButtonReleaseHandler      = fn; /* std::cref(InRun.xbutton): use XButton */
   }

   _force_inline_ void
   RegisterEventHandler (void (*fn)(XDestroyWindowEvent &))
   {  using namespace Detail;
      _handlers[DESTROY_NOTIFY] = DestroyNotifyProxy;
      DestroyNotifyHandler      = fn;
   }

   _force_inline_ void
   RegisterEventHandler (void (*fn)(XMapRequestEvent &))
   {  using namespace Detail;
      _handlers[MAP_REQUEST] = MapRequestProxy;
      MapRequestHandler      = fn;
   }

   _force_inline_ void
   RegisterEventHandler (void (*fn)(XConfigureRequestEvent &))
   {  using namespace Detail;
      _handlers[CONFIGURE_REQUEST] = ConfigureRequestProxy;
      ConfigureRequestHandler      = fn;
   }

   _force_inline_ void
   RegisterEventHandler (void (*fn)(XClientMessageEvent &))
   {  using namespace Detail;
      _handlers[CLIENT_MESSAGE] = ClientMessageProxy;
      ClientMessageHandler      = fn;
   }

   _force_inline_ void
   RegisterEventHandler (void (*fn)(XMappingEvent &))
   {  using namespace Detail;
      _handlers[MAPPING_NOTIFY] = MappingNotifyProxy;
      MappingNotifyHandler = fn;
   }

   template<typename FnSignature, bool Assert>
   _force_inline_ void
   RegisterEventHandler (FnSignature)
   {  static_assert(Assert, "Wrong function signature");  }

   template<bool Assert>
   _force_inline_ void
   RegisterEventHandler (void)
   {  static_assert(Assert, "Expect 1 argument at least");  }

   template<typename FnSign, typename ... Args>
   _force_inline_ void
   RegisterEventHandler (FnSign fn, Args ... args)
   {  RegisterEventHandler(fn);
      RegisterEventHandler(args...);
   }
}

} // ~ namespace X11
