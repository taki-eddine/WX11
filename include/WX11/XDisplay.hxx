////////////////////////////////////////////////////////////////////////////////
#pragma once
#include <Cxx/Array.hxx>
#include <Cxx/Math/BinaryCombination.hxx>

#include "WX11/intern/Xlib.h"
#include "WX11/Screen.hxx"
////////////////////////////////////////////////////////////////////////////////
namespace X11 {
using Cxx::Array;  //! @fixme Don't use `using' inside header

class Display final
{     using ClassName = Display;
   private:
      static priv::_XDisplay *_dpy;          //!< The connection  with the xserver.

      static Array<u32> _ignored_modifiers;  //!< Each index specify wether the modifier is ignored or not,
                                             //!<  use it when grabbing.

      static u32 _state_cleaner;             //!< This is the inverse of ignored modifiers, used to clean the state

      //! @todo Remove `_nscreens' beacause we can get it from `_screens.size'.
      static u32 _nscreens;                  //!< Number of real screen.
      static Array<Screen> _screens;         //!< List of screen.

   public:
      Array () = delete;
      static void StaticConstructor ();
      static void StaticDestructor  ();

      // ~ ---------------------------------------------------------------------
      void  SetScreensSnap (u32 snap);

      _force_inline_ Window
      DefaultRootWindow ()
      {  return 0/*X.Screens[X.DefaultScreen].Root*/;  }

      // ~ ---------------------------------------------------------------------
   /*
      _space_property_(Detail::_xserver.Fd,                ConnectionNumber);
      _space_property_(Detail::_xserver.DefaultScreen,     DefaultScreen);
      _space_property_(Detail::_xserver.Qlen,              QLength);
      //_space_property_(Detail::_xserver.NScreens,         ScreenCount);
      _space_property_(Detail::_xserver.Vendor,            ServerVendor);
      _space_property_(Detail::_xserver.ProtoMajorVersion, ProtocolVersion);
      _space_property_(Detail::_xserver.ProtoMinorVersion, ProtocolRevision);
      _space_property_(Detail::_xserver.Release,           VendorRelease);
   */
      _property_(Name, _dpy->display_name, static, read_only, get);
   /*
      _space_property_(Detail::_xserver.BitmapUnit,        BitmapUnit);
      _space_property_(Detail::_xserver.BitmapBitOrder,    BitmapBitOrder);
      _space_property_(Detail::_xserver.BitmapPad,         BitmapPad);
      _space_property_(Detail::_xserver.ByteOrder,         ImageByteOrder);
   // _space_property_(Detail::_xserver.Request,           NextRequest);
      _space_property_(Detail::_xserver.LastRequestRead,   LastKnownRequestProcessed);
   */

      //! @todo `IgnoredModifiers' is modifier not a property.
      //! @todo `_modifier_' macro is not stable and not ready for use specially it can't handle static members.
      //_modifier_(static, std::initializer_list<u32>, IgnoredModifiers);
      private:
         static _force_inline_ void setIgnoredModifiers (const std::initializer_list<u32> &value);

      public:
         static struct IgnoredModifiers final
         {  _force_inline_ auto &
            operator = (const std::initializer_list<u32> &value)
            {  setIgnoredModifiers (value);
               return *this;
            }
         } IgnoredModifiers;

      _property_(StateCleaner,     _state_cleaner,     static, read_only, get);
};

// ~ ---------------------------------------------------------------------------
_force_inline_ void
Display::setIgnoredModifiers (const std::initializer_list<u32> &list)
{  _ignored_modifiers = Cxx::Math::binaryCombination<u32>(list);

   _state_cleaner = 0u;
   for ( const u32 *it=list.begin(); it != list.end(); ++it)
   {  _state_cleaner |= *it;  }
   _state_cleaner = ~_state_cleaner;
}

} // ~ namespace X11
