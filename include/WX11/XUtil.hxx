#ifndef XUTIL_HXX
#define XUTIL_HXX

namespace X11
{  /*******************************************************************************************************************/
   /** New version containing base_width, base_height, and win_gravity fields;
    *  used with WM_NORMAL_HINTS.
   ********************************************************************************************************************/
   class XSizeHints
   {  public:
         long Flags;    /* marks which fields in this structure are defined */
         int  X,
              Y;        /* obsolete for new window mgrs, but clients */
         int  Width,
              Height;   /* should set so old wm's don't mess up */
         int  MinWidth,
              MinHeight;
         int  MaxWidth,
              MaxHeight;
         int  WidthInc,
              HeightInc;

         struct
         {  int X;   /* numerator */
            int Y;   /* denominator */
         } MinAspect, MaxAspect;

         int BaseWidth,
             BaseHeight;     /* added by ICCCM version 1 */
         int WinGravity;     /* added by ICCCM version 1 */
   };

   /*
   * The next block of definitions are for window manager properties that
   * clients and applications use for communication.
   */

   /* flags argument in size hints */
   enum SizeHintsFlags : long
   {  US_POSITION  = (1L << 0),  /* user specified x, y */
      US_SIZE      = (1L << 1),  /* user specified width, height */

      P_POSITION    = (1L << 2), /* program specified position */
      P_SIZE        = (1L << 3), /* program specified size */
      P_MIN_SIZE    = (1L << 4), /* program specified minimum size */
      P_MAX_SIZE    = (1L << 5), /* program specified maximum size */
      P_RESIZE_INC  = (1L << 6), /* program specified resize increments */
      P_ASPECT      = (1L << 7), /* program specified min and max aspect ratios */
      P_BASE_SIZE   = (1L << 8), /* program specified base for incrementing */
      P_WIN_GRAVITY = (1L << 9), /* program specified window gravity */

      /* obsolete */
      P_ALL_HINTS   = (P_POSITION|P_SIZE|P_MIN_SIZE|P_MAX_SIZE|P_RESIZE_INC|P_ASPECT)
   };
}

#endif // XUTIL_HXX
