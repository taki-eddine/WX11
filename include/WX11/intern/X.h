////////////////////////////////////////////////////////////////////////////////
#pragma once
////////////////////////////////////////////////////////////////////////////////
namespace X11  {
namespace priv {
extern "C" {

using XID      = unsigned long;
using Mask     = unsigned long;
using Atom     = unsigned long;
using VisualID = unsigned long;
using Time     = unsigned long;
using Window   = XID;
using Drawable = XID;
using Font     = XID;
using Pixmap   = XID;
using Cursor   = XID;
using Colormap = XID;
using GContext = XID;
using KeySym   = XID;
using KeyCode  = unsigned char;

//•━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ < RESERVED RESOURCE AND CONSTANT DEFINITIONS > ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━•//
enum
{  NONE             = 0L,  // universal null resource or null atom
   PARENT_RELATIVE  = 1L,  // background pixmap in CreateWindow and ChangeWindowAttributes
   COPY_FROM_PARENT = 0L,  /**
                            * border pixmap in CreateWindow and ChangeWindowAttributes
                            * special VisualID and special window
                            * class passed to CreateWindow
                           ************************************************************/
   POINTER_WINDOW    = 0L, // destination window in SendEvent
   INPUT_FOCUS       = 1L, // destination window in SendEvent
   POINTER_ROOT      = 1L, // focus window in SetInputFocus
   ANY_PROPERTY_TYPE = 0L, // special Atom, passed to GetProperty
   ANY_KEY           = 0L, // special Key Code, passed to GrabKey
   ANY_BUTTOM        = 0L, // special Button Code, passed to GrabButton
   ALL_TEMPORARY     = 0L, // special Resource ID passed to KillClient
   CURRENT_TIME      = 0L, // special Time
   NO_SYMBOL         = 0L  // special KeySym
};

//•━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ < EVENT DEFINITIONS > ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━•//
/**
 * @brief Input Event Masks.
 * Used as event-mask window attribute and as arguments to Grab requests.
 * Not to be confused with event names.
**/
enum EventMask : long
{  NO_EVENT_MASK              = 0L,
   KEY_PRESS_MASK             = (1L<<0),
   KEY_RELEASE_MASK           = (1L<<1),
   BUTTON_PRESS_MASK          = (1L<<2),
   BUTTON_RELEASE_MASK        = (1L<<3),
   ENTER_WINDOW_MASK          = (1L<<4),
   LEAVE_WINDOW_MASK          = (1L<<5),
   POINTER_MOTION_MASK        = (1L<<6),
   POINTER_MOTION_HINT_MASK   = (1L<<7),
   BUTTON1_MOTION_MASK        = (1L<<8),
   BUTTON2_MOTION_MASK        = (1L<<9),
   BUTTON3_MOTION_MASK        = (1L<<10),
   BUTTON4_MOTION_MASK        = (1L<<11),
   BUTTON5_MOTION_MASK        = (1L<<12),
   BUTTON_MOTION_MASK         = (1L<<13),
   KEYMAP_STATE_MASK          = (1L<<14),
   EXPOSURE_MASK              = (1L<<15),
   VISIBILITY_CHANGE_MASK     = (1L<<16),
   STRUCTURE_NOTIFY_MASK      = (1L<<17),
   RESIZE_REDIRECT_MASK       = (1L<<18),
   SUBSTRUCTURE_NOTIFY_MASK   = (1L<<19),
   SUBSTRUCTURE_REDIRECT_MASK = (1L<<20),
   FOCUS_CHANGE_MASK          = (1L<<21),
   PROPERTY_CHANGE_MASK       = (1L<<22),
   COLORMAP_CHANGE_MASK       = (1L<<23),
   OWNER_GRAB_BUTTON_MASK     = (1L<<24)
};

/**
 * @brief Event Types
 * Used in "type" field in XEvent structures.
 * Not to be confused with event masks above.
 * They start from 2 because 0 and 1 are reserved in the protocol for errors and replies.
**/
enum EventType : long
{  KEY_PRESS = 2,
   KEY_RELEASE,
   BUTTON_PRESS,
   BUTTON_RELEASE,
   MOTION_NOTIFY,
   ENTER_NOTIFY,
   LEAVE_NOTIFY,
   FOCUS_IN,
   FOCUS_OUT,
   KEYMAP_NOTIFY,
   EXPOSE,
   GRAPHICS_EXPOSE,
   NO_EXPOSE,
   VISIBILITY_NOTIFY,
   CREATE_NOTIFY,
   DESTROY_NOTIFY,
   UNMAP_NOTIFY,
   MAP_NOTIFY,
   MAP_REQUEST,
   REPARENT_NOTIFY,
   CONFIGURE_NOTIFY,
   CONFIGURE_REQUEST,
   GRAVITY_NOTIFY,
   RESIZE_REQUEST,
   CIRCULATE_NOTIFY,
   CIRCULATE_REQUEST,
   PROPERTY_NOTIFY,
   SELECTION_CLEAR,
   SELECTION_REQUEST,
   SELECTION_NOTIFY,
   COLORMAP_NOTIFY,
   CLIENT_MESSAGE,
   MAPPING_NOTIFY,
   GENERIC_EVENT,
   LAST_EVENT
};

/**
 * @brief Key masks.
 * Used as modifiers to GrabButton and GrabKey, results of QueryPointer,
 * state in various key-, mouse-, and button-related events.
**/
enum KeyMask
{  SHIFT_MASK    = (1<<0),
   CAPS_MASK     = (1<<1),
   CONTROL_MASK  = (1<<2),
   ALT_MASK      = (1<<3),
   NUM_LOCK_MASK = (1<<4),  // Under test: default MOD2Mask
   MOD3_MASK     = (1<<5),
   SUPER_MASK    = (1<<6),
   MOD5_MASK     = (1<<7)
};

/**
 * @brief Modifier names.
 * Used to build a SetModifierMapping request or to read a GetModifierMapping request.
 * These correspond to the masks defined above.
**/
enum ModifierIndex
{  SHIFT_MAP_INDEX   = 0,
   LOCK_MAP_INDEX    = 1,
   CONTROL_MAP_INDEX = 2,
   MOD1_MAP_INDEX    = 3,
   MOD2_MAP_INDEX    = 4,
   MOD3_MAP_INDEX    = 5,
   MOD4_MAP_INDEX    = 6,
   MOD5_MAP_INDEX    = 7
};

/**
 * @brief Button masks.
 * Used in same manner as Key masks above.
 * Not to be confused with button names below.
**/
enum ButtonMask
{  BUTTON1_MASK = (1<<8),
   BUTTON2_MASK = (1<<9),
   BUTTON3_MASK = (1<<10),
   BUTTON4_MASK = (1<<11),
   BUTTON5_MASK = (1<<12)
};

enum Modifier
{  ANY_MODIFIER = (1<<15)  // used in GrabButton, GrabKey
};

/**
 * @brief Mouse button names.
 * Used as arguments to GrabButton and as detail in ButtonPress and ButtonRelease events.
 * Not to be confused with button masks above.
 * Note that 0 is already defined above as "AnyButton".
**/
enum Mouse
{  BUTTON_1 = 1,
   BUTTON_2 = 2,
   BUTTON_3 = 3,
   BUTTON_4 = 4,
   BUTTON_5 = 5
};

/** @brief Notify modes */
enum NotifyModes
{  NOTIFY_NORMAL        = 0,
   NOTIFY_GRAB          = 1,
   NOTIFY_UNGRAB        = 2,
   NOTIFY_WHILE_GRABBED = 3,
   NOTIFY_HINT          = 1  // For MotionNotify events.
};

/** @brief Notify detail */
enum NotifyDetail
{  NOTIFY_ANCESTOR          = 0,
   NOTIFY_VIRTUAL           = 1,
   NOTIFY_INFERIOR          = 2,
   NOTIFY_NONLINEAR         = 3,
   NOTIFY_NONLINEAR_VIRTUAL = 4,
   NOTIFY_POINTER           = 5,
   NOTIFY_POINTER_ROOT      = 6,
   NOTIFY_DETAIL_NONE       = 7
};

/** @brief Visibility notify */
enum VisibilityNotify
{  VISIBILITY_UNOBSCURED         = 0,
   VISIBILITY_PARTIALLY_OBSCURED = 1,
   VISIBILITY_FULLY_OBSCURED     = 2
};

/** @brief Circulation request */
enum CirculationRequest
{  PLACE_ON_TOP    = 0,
   PLACE_ON_BOTTOM = 1
};

/** @brief Protocol families */
enum ProtocolFamily
{  FAMILY_INTERNET  = 0,  // IPv4
   FAMILY_DECNET    = 1,
   FAMILY_CHAOS     = 2,
   FAMILY_INTERNET6 = 6   // IPv6
};

enum
{  FAMILY_SERVER_INTERPRETED = 5  // authentication families not tied to a specific protocol
};

/** @brief Property notification */
enum PropertyNotification
{  PROPERTY_NEW_VALUE = 0,
   PROPERTY_DELETE    = 1
};

/** @brief Color Map notification */
enum ColorMapNotification
{  COLORMAP_UNINSTALLED = 0,
   COLORMAP_INSTALLED   = 1
};

/** @brief GrabPointer, GrabButton, GrabKeyboard, GrabKey Modes */
enum GrabMode
{  GRAB_MODE_SYNC  = 0,
   GRAB_MODE_ASYNC = 1
};

/** @brief GrabPointer, GrabKeyboard reply status */
enum GrabReplyStatus
{  GRAB_SUCCESS      = 0,
   ALREADY_GRABBED   = 1,
   GRAB_INVALID_TIME = 2,
   GRAB_NOT_VIEWABLE = 3,
   GRAB_FROZEN       = 4
};

/** @brief AllowEvents modes */
enum AllowEvent
{  ASYNC_POINTER   = 0,
   SYNC_POINTER    = 1,
   REPLAY_POINTER  = 2,
   ASYNC_KEYBOARD  = 3,
   SYNC_KEYBOARD   = 4,
   REPLAY_KEYBOARD = 5,
   ASYNC_BOTH      = 6,
   SYNC_BOTH       = 7
};

/** @brief Used in SetInputFocus, GetInputFocus */
enum RevertFocusTo
{  REVERT_TO_NONE               = (int)NONE,
   REVERT_TO_POINTER_ROOT       = (int)POINTER_ROOT,
   REVERT_TO_PARENT             = 2
};

enum QueueStatus
{  QUEUED_ALREADY       = 0,
   QUEUED_AFTER_READING = 1,
   QUEUED_AFTER_FLUSH   = 2,
};

//•━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ < ERROR CODES > ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━•//
enum ErrorCode
{  SUCCESS               = 0,   // everything's okay */
   BAD_REQUEST           = 1,   // bad request code */
   BAD_VALUE             = 2,   // int parameter out of range */
   BAD_WINDOW            = 3,   // parameter not a _Window */
   BAD_PIXMAP            = 4,   // parameter not a _Pixmap */
   BAD_ATOM              = 5,   // parameter not an _Atom */
   BAD_CURSOR            = 6,   // parameter not a _Cursor */
   BAD_FONT              = 7,   // parameter not a _Font */
   BAD_MATCH             = 8,   // parameter mismatch */
   BAD_DRAWABLE          = 9,   // parameter not a _Pixmap or _Window */
   BAD_ACCESS            = 10,  /**
                                 * Depending on context:
                                 * - key/button already grabbed
                                 * - attempt to free an illegal cmap entry
                                 * - attempt to store into a read-only color map entry.
                                 * - attempt to modify the access control list from other than the local host.
                                *******************************************************************************/
   BAD_ALLOC             = 11,  // insufficient resources */
   BAD_COLOR             = 12,  // no such colormap */
   BADGC                 = 13,  // parameter not a GC */
   BADID_CHOICE          = 14,  // choice not in range or already used */
   BAD_NAME              = 15,  // font or color name doesn't exist */
   BAD_LENGTH            = 16,  // Request length incorrect */
   BAD_IMPLEMENTATION    = 17,  // server is defective */
   FIRST_EXTENSION_ERROR = 128,
   LAST_EXTENSION_ERROR  = 255
};

//•━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ < WINDOW DEFINITIONS > ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━•//
/* Window classes used by CreateWindow */
/* Note that CopyFromParent is already defined as 0 above */
enum WindowClass
{  INPUT_OUTPUT = 1,
   INPUT_ONLY   = 2,
};

/* Window attributes for CreateWindow and ChangeWindowAttributes */
enum WindowAttributes
{  CW_BACK_PIXMAP       = (1L<<0),
   CW_BACK_PIXEL        = (1L<<1),
   CW_BORDER_PIXMAP     = (1L<<2),
   CW_BORDER_PIXEL      = (1L<<3),
   CW_BIT_GRAVITY       = (1L<<4),
   CW_WIN_GRAVITY       = (1L<<5),
   CW_BACKING_STORE     = (1L<<6),
   CW_BACKING_PLANES    = (1L<<7),
   CW_BACKING_PIXEL     = (1L<<8),
   CW_OVERRIDE_REDIRECT = (1L<<9),
   CW_SAVE_UNDER        = (1L<<10),
   CW_EVENT_MASK        = (1L<<11),
   CW_DONT_PROPAGATE    = (1L<<12),
   CW_COLORMAP          = (1L<<13),
   CW_CURSOR            = (1L<<14)
};

/* ConfigureWindow structure */
enum WindowConfigure
{  CWX             = (1<<0),
   CWY             = (1<<1),
   CW_WIDTH        = (1<<2),
   CW_HEIGHT       = (1<<3),
   CW_BORDER_WIDTH = (1<<4),
   CW_SIBLING      = (1<<5),
   CW_STACK_MODE   = (1<<6)
};

/* Bit Gravity */
enum Gravity
{  FORGET_GRAVITY     = 0,
   NORTH_WEST_GRAVITY = 1,
   NORTH_GRAVITY      = 2,
   NORTH_EAST_GRAVITY = 3,
   WEST_GRAVITY       = 4,
   CENTER_GRAVITY     = 5,
   EAST_GRAVITY       = 6,
   SOUTH_WEST_GRAVITY = 7,
   SOUTH_GRAVITY      = 8,
   SOUTH_EAST_GRAVITY = 9,
   STATIC_GRAVITY     = 10,
   UNMAP_GRAVITY      = 0    /* Window gravity + bit gravity above */
};

/* Used in CreateWindow for backing-store hint */
enum CreateWindowHint
{  NOT_USEFUL  = 0,
   WHEN_MAPPED = 1,
   ALWAYS      = 2
};

/* Used in GetWindowAttributes reply */
enum GetWindowAttributesReply
{  IS_UNMAPPED   = 0,
   IS_UNVIEWABLE = 1,
   IS_VIEWABLE   = 2
};

/* Used in ChangeSaveSet */
enum SaveSetMode
{  SET_MODE_INSERT = 0,
   SET_MODE_DELETE = 1,
};

/* Used in ChangeCloseDownMode */
enum CloseDownMode
{  DESTROY_ALL      = 0,
   RETAIN_PERMANENT = 1,
   RETAIN_TEMPORARY = 2
};

/* Window stacking method (in configureWindow) */
enum WindowStackingMethod
{  ABOVE     = 0,
   BELOW     = 1,
   TOP_IF    = 2,
   BOTTOM_IF = 3,
   OPPOSITE  = 4
};

/* Circulation direction */
enum CiculationDirection
{  RAISE_LOWEST  = 0,
   LOWER_HIGHEST = 1
};

/* Property modes */
enum PropertyMode
{  PROP_MODE_REPLACE = 0,
   PROP_MODE_PREPEND = 1,
   PROP_MODE_APPEND  = 2
};

enum Direction
{  Center = 0x1FFF0000,
   Right  = 0x1FFF0001,
   Left   = 0x1FFF0002,
   Top    = 0x1FFF0003,
   Bottom = 0x1FFF0004
};

} // ~ extern "C"
} // ~ namespace priv
} // ~ namespace X11
