////////////////////////////////////////////////////////////////////////////////
#pragma once
#include <Cxx/Array.hxx>
#include "WX11/XLib.hxx"
#include "WX11/Screen.hxx"
#include "WX11/XServer.hxx"
////////////////////////////////////////////////////////////////////////////////
extern "C" {
typedef struct _XDisplay XDisplay;
}

namespace X11 {
using Cxx::Array;

class Display final
{     using ClassName = Display;
      virtual void _ () = 0;  // - abstract

   private:
      static XDisplay *_dpy;             //!< Connection to X server.

      static u32 _ignored_modifiers[9];  /** Each index specify wether the modifier is ignored or not,
                                          *  use it when grabbing.
                                         ******************************************************************/
      static u32 _state_cleaner;         // This is the inverse of ignored modifiers, used to clean the state
      static u32 _nscreens;
      static Array<Screen> _screens;

   public:
      static void StaticConstructor ();
      static void StaticDestructor  ();

      static struct IgnoredModifiers final
      {  /*_force_inline_ u32
         operator [] (u32 mod_index)
         {  using _private_space_;
            return _ignored_modifiers[mod_index];
         }

         _force_inline_ IgnoredModifiers &
         operator = (u32 mods)
         {  using _private_space_;
            u32  nmods = 0;

            // @bug we must create a combinison of modifier, means create all possibilies
            for (u32 i=0, mod=1; i < 9; ++i, mod<<=1)
            {  _ignored_modifiers[i] = 0;  // Reset the block before using it
               if ( mod & mods )
               {  _ignored_modifiers[nmods] = mod;
                  ++nmods;
               }
            }
            _state_cleaner = ~(_ignored_modifiers[nmods] = mods);
            return *this;
         }*/
      } IgnoredModifiers;

      // ~ ---------------------------------------------------------------------
      void  SetScreensSnap (u32 snap);

      _force_inline_ Window
      DefaultRootWindow ()
      {  return 0/*X.Screens[X.DefaultScreen].Root*/;  }

      // ~ ---------------------------------------------------------------------
   /*
      _space_property_(Detail::_xserver.Fd,                ConnectionNumber);
      _space_property_(Detail::_xserver.DefaultScreen,     DefaultScreen);
      _space_property_(Detail::_xserver.Qlen,              QLength);
      //_space_property_(Detail::_xserver.NScreens,         ScreenCount);
      _space_property_(Detail::_xserver.Vendor,            ServerVendor);
      _space_property_(Detail::_xserver.ProtoMajorVersion, ProtocolVersion);
      _space_property_(Detail::_xserver.ProtoMinorVersion, ProtocolRevision);
      _space_property_(Detail::_xserver.Release,           VendorRelease);
   */
   // _space_property_(X.DisplayName,       Name);
   /*
      _space_property_(Detail::_xserver.BitmapUnit,        BitmapUnit);
      _space_property_(Detail::_xserver.BitmapBitOrder,    BitmapBitOrder);
      _space_property_(Detail::_xserver.BitmapPad,         BitmapPad);
      _space_property_(Detail::_xserver.ByteOrder,         ImageByteOrder);
   // _space_property_(Detail::_xserver.Request,           NextRequest);
      _space_property_(Detail::_xserver.LastRequestRead,   LastKnownRequestProcessed);

      _space_property_(Detail::_state_cleaner, StateCleaner);
   */
};

} // ~ namspace X11
