////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma once
#include "X.hxx"
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace X11 {

using XPointer = char *;
using Status   = int;

/**
 * @brief Extensions need a way to hang private data on some structures.
**/
class XExtData
{  public:
      int       number;                                 /* number returned by XRegisterExtension */
      XExtData  *next;                                  /* next item on list of data for structure */
      int       (*free_private) (XExtData *extension);  /* called to free private storage */
      XPointer  private_data;                           /* data private to this extension. */
};

/**
 * @brief Graphics context.
 *  The contents of this structure are implementation dependent.
 *  A GC should be treated as opaque by application code.
**/
class XGC
{   public:
       //XExtData *ext_data;  /* hook for extension to hang data */
       //GContext gid;        /* protocol ID for graphics context */
       /* there is more to this structure, but it is private to Xlib */
};
using GC = XGC *;

/**
 * @brief Visual structure; contains information about colormapping possible.
**/
class Visual
{  public:
      XExtData      *ext_data;   /* hook for extension to hang data */
      VisualID      visualid;   /* visual id of this visual */
      int           c_class;      /* C++ class of screen (monochrome, etc.) */
      unsigned long red_mask, green_mask, blue_mask;   /* mask values */
      int           bits_per_rgb;   /* log base 2 of distinct color values */
      int           map_entries;   /* color map entries */
};

/**
 * @brief Depth structure; contains information for each possible depth.
**/
class XDepth
{  public:
      int     depth;     /* this depth (Z) of the depth */
      int     nvisuals;  /* number of Visual types at this depth */
      Visual  *visuals;  /* list of visuals possible at this depth */
};

struct XPrivate;
struct XrmHashBucketRec;

///using XErrorHandler = Detail::XErrorHandlerFn;

} // ~ namespace X11
